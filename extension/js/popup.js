var helper = 
{
	extractDomain: function(url) 
	{
		var parser = document.createElement('a');
		parser.href = url;
	
		return parser.hostname;
	},
	
	extractElement: function(list, predicate)
	{
		for (var i = 0; i < list.length; i++)
		{
			if(predicate(list[i]))
				return list[i];
		}
	
		return null;
	}
};

function banDomain()
{
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs)
	{
		var domain = helper.extractDomain(tabs[0].url);

		chrome.storage.local.get({ bannedDomains: [] }, function(result)
		{
			var bannedDomains = result.bannedDomains;
			var banned = helper.extractElement(bannedDomains, x => x === domain);

			if(banned === null)
			{
				if(domain.includes("www."))
					bannedDomains.push(domain.split("www.")[1]);
				else
					bannedDomains.push("www." + domain);

				bannedDomains.push(domain);
			}

			chrome.storage.local.set({ bannedDomains: bannedDomains }, function()
			{
				if(chrome.runtime.lastError)
				{
					console.log("Error: could not ban domain.");
					console.log(chrome.runtime.lastError);
				}

				chrome.tabs.reload(tabs[0].id);
			});
		});
	});
}

function setStatusUI(uiText, uiBtn, status)
{
	uiText.textContent = status;
	uiText.className = status.toLowerCase();

	if(status === "Running")
		uiBtn.textContent = "Stop";
	else
		uiBtn.textContent = "Start";
}

document.addEventListener('DOMContentLoaded', function()
{
	var btnAdd = document.getElementById('btn-add');
	var btnSettings = document.getElementById('btn-settings');
	var btnDonate = document.getElementById('btn-donate');
	var btnToggleStatus =  document.getElementById('btn-toggle-status');
	var textStatus =  document.getElementById('text-status');

	btnAdd.addEventListener('click', banDomain);
	btnSettings.addEventListener('click', function() { chrome.tabs.create({ url: "view/settings.html" }); });
	btnDonate.addEventListener("click", function() { chrome.runtime.sendMessage({type:"donate"}); });

	chrome.storage.local.get({status:"Running"}, function(result)
	{
		status = result.status;

		setStatusUI(textStatus, btnToggleStatus, status);
	});

	btnToggleStatus.addEventListener("click", function()
	{
		chrome.storage.local.get({status:"Running"}, function(result)
		{
			status = result.status;

			if(status === "Running")
			{
				chrome.storage.local.set({ status:"Stopped" }, function()
				{
					if(chrome.runtime.lastError)
					{
						console.log("Error: could not stop the service.");
						console.log(chrome.runtime.lastError);
					}

					setStatusUI(textStatus, btnToggleStatus, "Stopped");
				});
			}
			else
			{
				chrome.storage.local.set({ status:"Running" }, function()
				{
					if(chrome.runtime.lastError)
					{
						console.log("Error: could not start the service.");
						console.log(chrome.runtime.lastError);
					}

					setStatusUI(textStatus, btnToggleStatus, "Running");
				});
			}
		});
	});
});